//
// Created by liyanc on 8/14/18.
//

#include <iostream>
#include <lz4.h>
#include "Device.h"
#include "Utilities.h"

tbb::atomic<bool> Device::should_run(false);

std::map<std::string, int> Device::serial2id_map{
    {"001616770547", 1}, {"004395742047", 2}, {"001612470547", 0}
};



Device::Device(libfreenect2::Freenect2 *dri_cntxt, int ind, std::string sn,
               TClock::time_point t0)
{
    dev_ind = ind;
    serialnum = std::move(sn);
    is_open = false;
    is_running = false;
    start = t0;
    driver_context = dri_cntxt;
    std::cout << "Instantiated device " << serialnum << std::endl;
}

bool Device::openDevice()
{
    std::cout << "Start opening device " << serialnum << std::endl;

    dev_pipline = new libfreenect2::CudaPacketPipeline();
    dev_pipline = new libfreenect2::CudaDumpRgbPacketPipeline();
    if (!dev_pipline)
    {
        std::cout << "Pipeline initialization failed\n";
        throw "Pipeline initialization failed\n";
    }

    uint frame_types = libfreenect2::Frame::Color | libfreenect2::Frame::Depth;
    frame_source = new libfreenect2::SyncMultiFrameListener(frame_types);

    dev_handle = driver_context->openDevice(serialnum, dev_pipline);

    if (!(is_open = (dev_handle != 0))) return false;


    return true;
}

bool Device::startCapture() {
    should_run = true;

    dev_handle->setColorFrameListener(frame_source);
    dev_handle->setIrAndDepthFrameListener(frame_source);

    chck(dev_handle->start(), serialnum + "start error");

    std::cout << "Capture ready\n";

    is_running = true;
    return true;
}

bool Device::operator()(TimedFrame &t_frame)
{
    if (!is_running) startCapture();

    if (!should_run)
    {
        closeDevice();
        return false;
    }

    t_frame.kinect_ind = dev_ind;

    frame_source->waitForNewFrame(frame_buf);
    t_frame.timestamp = TClock::now() - start;

    // Extract timestamps and other meta data from the frame buffer
    auto rgb = frame_buf[libfreenect2::Frame::Color];
    auto depth = frame_buf[libfreenect2::Frame::Depth];

    t_frame.raw_t_color = rgb->timestamp;
    t_frame.raw_t_depth = depth->timestamp;

    t_frame.color_h = rgb->height;
    t_frame.color_w = rgb->width;
    t_frame.depth_h = depth->height;
    t_frame.depth_w = depth->width;

    // Get length of buffers and copy to a managed pointer
    size_t color_buf_len = rgb->bytes_per_pixel;
    size_t depth_buf_len = depth->width * depth->height * depth->bytes_per_pixel;
    t_frame.jpeg_buf_len = color_buf_len;

    shared_ptr<uchar[]> color_buf_cpy(new uchar[color_buf_len]);
    shared_ptr<uchar[]> depth_buf_cpy(new uchar[depth_buf_len]);

    memcpy(color_buf_cpy.get(), rgb->data, color_buf_len);
    memcpy(depth_buf_cpy.get(), depth->data, depth_buf_len);

    t_frame.color_buf = color_buf_cpy;
    t_frame.depth_buf = depth_buf_cpy;

    // Release the frame_buf after copying the content
    frame_source->release(frame_buf);

    return true;
}

//cv::Mat color_ptr(rbg->height, rbg->width, CV_8UC4, rbg->data);
//cv::Mat depth_ptr(depth->height, depth->width, CV_32FC1, depth->data);

Device::~Device()
{
    closeDevice();
}

const string &Device::getSerialnum() const {
    return serialnum;
}

void Device::setDev_ind(int dev_ind) {
    Device::dev_ind = dev_ind;
}

void Device::closeDevice()
{
    if (!should_run && is_running)
    {
        dev_handle->stop();
        dev_handle->close();
        is_running = false;
    }

}

void Device::setShould_run(bool should_run) {
    Device::should_run = should_run;
}

