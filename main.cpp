//
// Created by liyanc on 8/14/18.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include <tbb/tbb.h>
#include <tbb/flow_graph.h>

#include "Driver.h"
#include "Device.h"
#include "DeviceFlagHandler.h"
#include "TimestampWriter.h"
#include "ImgWriter.h"


int main()
{
    std::string sess_name;
    std::string rt_path("/media/cvlabliyan/Capture");
    Driver driver{nullptr};
    tbb::task_scheduler_init init(24);
    tbb::flow::graph g;
    std::vector<std::shared_ptr<Device>> dev_vec;
    std::vector<tbb::flow::source_node<TimedFrame>> src_node_vec;


    //Initialize driver and register interrupt handler for ending and cleaning
    driver.instantiateDevices(dev_vec);
    enableDevice();
    register_sigint();

    //Setup capture session name
    std::cout << "Session name:\n";
    std::cin >> sess_name;


    ImgWriterDumpJpeg img_writer(rt_path, sess_name);
    TimestampWriter t_writer(rt_path + "/" + sess_name + "_timestamp.txt");

    tbb::flow::function_node<TimedFrame, int, tbb::flow::queueing> t_writer_n(g, 1, t_writer);
    tbb::flow::function_node<TimedFrame, int, tbb::flow::queueing> img_writer_n(g, 3, img_writer);
    ImgWriterDumpJpeg::startWriterThreads(1);

    //Setup capture device serial number to device id mapping
    for (auto &i : dev_vec)
    {
        int remapped_id = 0;
        remapped_id = Device::serial2id_map[i->getSerialnum()];

        i->setDev_ind(remapped_id);
        i->openDevice();

        src_node_vec.emplace_back(g, *i, false);
        std::cout << "Opened Device " << i->getSerialnum() << " as ID " << remapped_id << std::endl;
    }

    //Activate capturing after completing the graph
    for (auto &node : src_node_vec)
    {
        tbb::flow::make_edge(node, img_writer_n);
        tbb::flow::make_edge(node, t_writer_n);
    }

    for (auto &node: src_node_vec)
        node.activate();

    //Main loop and post-work cleanup
    g.wait_for_all();
    t_writer.closeFile();
    ImgWriter::stopWriterThreads();

    return 0;
}

/*
struct T {
    std::string name;
    vector<int> content;
};

int main()
{
     T entry{"Showtime: ", {0, 1, 2, 3, 4}};
     vector<T> buf;

     buf.emplace_back(std::move(entry));
     std::cout << buf[0].name;
     copy(buf[0].content.begin(), buf[0].content.end(), ostream_iterator<int>(cout, " "));
     std::cout << "\n";
     std::cout << entry.name;
     copy(entry.content.begin(), entry.content.end(), ostream_iterator<int>(cout, " "));
}
 */