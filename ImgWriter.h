//
// Created by liyanc on 8/15/18.
//

#ifndef KINECT_CAPTURE_IMGWRITER_H
#define KINECT_CAPTURE_IMGWRITER_H

#include <iostream>
#include <string>
#include <thread>
#include <opencv2/opencv.hpp>
#include <tbb/atomic.h>
#include <tbb/concurrent_queue.h>

#include "Device.h"

struct FileEntry {
    std::string filename;
    std::shared_ptr<char[]> content_buf;
    long bufsize;
};

class ImgWriter {

protected:
    std::string parent_dir;
    std::string session_name;
    std::string work_dir;
    static std::vector<std::thread> writer_pool;
    static tbb::atomic<long> counter;
    static tbb::concurrent_queue<FileEntry> write_buf;

public:

    ImgWriter(const std::string &parent_dir, const std::string &sess);

    std::string now();

    static void startWriterThreads(int num);

    static void stopWriterThreads();

    int operator() (const TimedFrame & in_frame);
};

class ImgWriterDumpJpeg: public ImgWriter{
public:
    ImgWriterDumpJpeg(const std::string &parentDir, const std::string &sess);

    int operator() (const TimedFrame & in_frame);
};

#endif //KINECT_CAPTURE_IMGWRITER_H
