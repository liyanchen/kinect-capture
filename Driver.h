//
// Created by liyanc on 8/14/18.
//

#ifndef KINECT_CAPTURE_KINECT_H
#define KINECT_CAPTURE_KINECT_H

#include <vector>
#include <memory>

#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener.hpp>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>

#include "Device.h"

using namespace std;

class Driver {

private:
    long num_device;
    libfreenect2::Freenect2 driver_context;
    libfreenect2::Logger * logger;
    vector<string> serialnum_vec;

public:
    Driver(libfreenect2::Logger * log_ptr);

    void instantiateDevices(vector<std::shared_ptr<Device>> &devp_vec);


};


#endif //KINECT_CAPTURE_KINECT_H
