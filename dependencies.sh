sudo apt install -y build-essential cmake pkg-config
sudo apt install -y libusb-1.0-0-dev
sudo apt install -y libturbojpeg libjpeg-turbo8-dev
sudo apt install -y libglfw3-dev
sudo apt install -y beignet-dev
sudo echo 64 > /sys/module/usbcore/parameters/usbfs_memory_mb

cd
git clone https://github.com/justusc/FindTBB.git
git clone https://github.com/OpenKinect/libfreenect2.git
cd libfreenect2
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=~/freenect2
make
make install

sudo cp ../platform/linux/udev/90-kinect2.rules /etc/udev/rules.d/


