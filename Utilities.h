//
// Created by liyanc on 8/14/18.
//

#ifndef KINECT_CAPTURE_UTILITIES_H
#define KINECT_CAPTURE_UTILITIES_H

#include <cstdio>
#include <string>

using namespace std;

void chck(bool status, const char * msg)
{
    if (!status)
    {
        printf("%s\n", msg);
        throw msg;
        exit(-1);
    }
}

void chck(bool status, const std::string & msg)
{
    if (!status)
    {
        printf("%s\n", msg.c_str());
        throw msg.c_str();
        exit(-1);
    }
}

#endif //KINECT_CAPTURE_UTILITIES_H
