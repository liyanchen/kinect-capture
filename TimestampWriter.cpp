//
// Created by liyanc on 8/15/18.
//

#include "TimestampWriter.h"

TimestampWriter::TimestampWriter(const std::string &filename)
{
    if (!filename.empty()) file = fopen(filename.c_str(), "w+");
    else file = 0;
    printf("File initialized as %ld\n", long(file));
}

int TimestampWriter::operator()(const TimedFrame &in_frame)
{
    long rawtime = boost::chrono::duration_cast<boost::chrono::milliseconds>(in_frame.timestamp).count();
    float color_time = in_frame.raw_t_color * 1e-4f;
    float depth_time = in_frame.raw_t_depth * 1e-4f;
    float time = rawtime * 1e-3f;
    if (!file) std::cout << "Global Timestamp: " << time << " Raw: " << color_time << " " << depth_time << std::endl;
    else {
        int rtn = fprintf(
                file, "%02ld %08.3f %09ld %f %f\n", in_frame.kinect_ind, time, rawtime, color_time, depth_time);
        //printf("Writing timestamps %d\n", rtn);
    }

    return 0;
}


/*
TimestampWriter::~TimestampWriter()
{
}
 */

void TimestampWriter::closeFile()
{
    if (file)
    {
        fclose(file);
        file = 0;
    }
}
