//
// Created by liyanc on 8/24/18.
//

#ifndef KINECT_CAPTURE_DEVICEFLAGHANDLER_H
#define KINECT_CAPTURE_DEVICEFLAGHANDLER_H

#include "Device.h"
#include "ImgWriter.h"
#include "csignal"

void inline enableDevice()
{
    Device::setShould_run(true);
}

void inline disableDevice()
{
    Device::setShould_run(false);
}

void sigint_handler(int s)
{
    disableDevice();
}

void register_sigint()
{
    signal(SIGINT, sigint_handler);
    signal(SIGTERM, sigint_handler);
    signal(SIGQUIT, sigint_handler);
}

#endif //KINECT_CAPTURE_DEVICEFLAGHANDLER_H
