//
// Created by liyanc on 8/14/18.
//

#include <iostream>
#include <chrono>
#include <memory>
#include <boost/chrono.hpp>
#include "Driver.h"

Driver::Driver(libfreenect2::Logger * log_ptr)
{
    auto ErrFlag = libfreenect2::Logger::Error;

    // Set freenect logging
    if (log_ptr == nullptr) log_ptr = libfreenect2::createConsoleLogger(libfreenect2::Logger::Debug);
    logger = log_ptr;
    libfreenect2::setGlobalLogger(logger);

    if ((num_device = driver_context.enumerateDevices()) == 0)
    {
        logger->log(ErrFlag, "No device connected");
        return;
    }

    for (int i = 0; i < num_device; ++i)
        serialnum_vec.push_back(driver_context.getDeviceSerialNumber(i));

}

void Driver::instantiateDevices(vector<std::shared_ptr<Device>> &devp_vec)
{
    auto now = TClock::now();
    for (int i = 0; i < num_device; ++i)
        devp_vec.emplace_back(make_unique<Device>(&driver_context, i, serialnum_vec[i], now));

    std::cout << "All devices instantiated\n";
}
