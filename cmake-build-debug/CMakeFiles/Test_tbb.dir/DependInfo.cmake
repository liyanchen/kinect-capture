# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/cvlabliyan/Desktop/kinect-capture/Device.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/Device.cpp.o"
  "/home/cvlabliyan/Desktop/kinect-capture/Driver.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/Driver.cpp.o"
  "/home/cvlabliyan/Desktop/kinect-capture/FPS_profiler.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/FPS_profiler.cpp.o"
  "/home/cvlabliyan/Desktop/kinect-capture/ImgWriter.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/ImgWriter.cpp.o"
  "/home/cvlabliyan/Desktop/kinect-capture/TimestampWriter.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/TimestampWriter.cpp.o"
  "/home/cvlabliyan/libfreenect2/src/flextGL.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/home/cvlabliyan/libfreenect2/src/flextGL.cpp.o"
  "/home/cvlabliyan/Desktop/kinect-capture/main.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/main.cpp.o"
  "/home/cvlabliyan/Desktop/kinect-capture/viewer.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Test_tbb.dir/viewer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EXAMPLES_WITH_OPENGL_SUPPORT=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/cvlabliyan/freenect2/include"
  "/usr/include/libdrm"
  "/home/cvlabliyan/libfreenect2/src"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
