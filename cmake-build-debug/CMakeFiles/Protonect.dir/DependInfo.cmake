# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/cvlabliyan/Desktop/kinect-capture/Protonect.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Protonect.dir/Protonect.cpp.o"
  "/home/cvlabliyan/libfreenect2/src/flextGL.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Protonect.dir/home/cvlabliyan/libfreenect2/src/flextGL.cpp.o"
  "/home/cvlabliyan/Desktop/kinect-capture/viewer.cpp" "/home/cvlabliyan/Desktop/kinect-capture/cmake-build-debug/CMakeFiles/Protonect.dir/viewer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EXAMPLES_WITH_OPENGL_SUPPORT=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/cvlabliyan/freenect2/include"
  "/usr/include/libdrm"
  "/home/cvlabliyan/libfreenect2/src"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
