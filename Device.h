//
// Created by liyanc on 8/14/18.
//

#ifndef KINECT_CAPTURE_DEVICE_H
#define KINECT_CAPTURE_DEVICE_H

#include <string>
#include <chrono>
#include <map>

#include <boost/chrono.hpp>
#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener.hpp>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>
#include <libfreenect2/frame_listener_impl.h>
#include <tbb/flow_graph.h>
#include <opencv2/opencv.hpp>

typedef boost::chrono::steady_clock TClock;

struct TimedFrame {
    TClock::duration timestamp;
    std::shared_ptr<uchar[]> color_buf, depth_buf;
    uint32_t raw_t_color, raw_t_depth, jpeg_buf_len;
    size_t color_w, color_h, depth_w, depth_h;
    long kinect_ind;
};

class Device {

private:

    int dev_ind;
    bool is_open, is_running;

    std::string serialnum;
    TClock::time_point start;
    libfreenect2::Freenect2 * driver_context;
    libfreenect2::PacketPipeline * dev_pipline;
    libfreenect2::Freenect2Device * dev_handle;
    libfreenect2::SyncMultiFrameListener * frame_source;
    libfreenect2::FrameMap frame_buf;

public:
    static tbb::atomic<bool> should_run;
    static std::map<std::string, int> serial2id_map;

    static void setShould_run(bool should_run);

    Device(libfreenect2::Freenect2 *dri_cntxt, int ind, std::string sn,
            TClock::time_point t0);

    bool openDevice();

    bool startCapture();

    bool operator()(TimedFrame &t_frame);

    const std::string &getSerialnum() const;

    void setDev_ind(int dev_ind);

    void closeDevice();

    virtual ~Device();

};


#endif //KINECT_CAPTURE_DEVICE_H
