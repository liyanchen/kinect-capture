//
// Created by liyanc on 8/15/18.
//

#include <chrono>
#include <cstdio>
#include <ctime>
#include <cstring>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <lz4.h>
#include <memory>

#include "ImgWriter.h"

tbb::atomic<long> ImgWriter::counter = 0;
tbb::concurrent_queue<FileEntry> ImgWriter::write_buf{};
std::vector<std::thread> ImgWriter::writer_pool{};

ImgWriter::ImgWriter(const std::string &parent_dir, const std::string &sess) :
    parent_dir(parent_dir),
    session_name(sess),
    work_dir(parent_dir + "/" + session_name){
    if (mkdir(work_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))
    {
        std::cout << "Can't create " << work_dir << std::endl;
        throw work_dir;
    }
}

std::string ImgWriter::now()
{
    char s[1024];
    struct timeb timebuffer;
    struct tm * lnow;
    ftime(&timebuffer);
    time_t * t_ptr = &(timebuffer.time); //the same like struct time_t

    lnow = localtime(t_ptr);
    sprintf(s, "-%02d-%02d-%02d-%03d-", lnow->tm_hour, lnow->tm_min, lnow->tm_sec, timebuffer.millitm);
    return std::string(s);
}

int ImgWriter::operator()(const TimedFrame &in_frame)
{
    char fname_color[32], fname_depth[32];
    std::string color_ext(".bmp");
    std::string depth_ext(".tiff");
    std::string compr_ext(".lz4");

    long current_cnt = ++counter;
    long time = boost::chrono::duration_cast<boost::chrono::milliseconds>(in_frame.timestamp).count();

    sprintf(fname_color, "%02ld-%09ld-%09ld", in_frame.kinect_ind, time, current_cnt);
    sprintf(fname_depth, "%02ld-%09ld-%09ld", in_frame.kinect_ind, time, current_cnt);

    std::string rgb_filename = work_dir + "/" + fname_color + color_ext + compr_ext;
    std::string dep_filename = work_dir + "/" + fname_depth + "_dep" + depth_ext;

    //FileEntry color_file{rgb_filename, {}};
    //FileEntry depth_file{dep_filename, {}};

    cv::Mat color_ref(in_frame.color_h, in_frame.color_w, CV_8UC4, in_frame.color_buf.get());
    cv::Mat depth_ref(in_frame.depth_h, in_frame.depth_w, CV_32FC1, in_frame.depth_buf.get());

    //Convert frames to writing format
    cv::Mat color, depth;
    cv::cvtColor(color_ref, color, cv::COLOR_RGBA2RGB);
    depth_ref.convertTo(depth, CV_16U);

    // Directly write to file
    //cv::imwrite(rgb_filename, color, {CV_IMWRITE_PNG_COMPRESSION, 1});
    //cv::imwrite(dep_filename, depth);

    // Write encoded bytestreams to queue
    // Magic number for 2^18 * 9.3125 and 2^14 * 4.25

    //color_file.content.reserve(2441216);
    //depth_file.content.reserve(69632);
    std::vector<uchar> color_bmp_buf, depth_enc_buf;
    color_bmp_buf.reserve(2441216 * 3);
    depth_enc_buf.reserve(69632);

    cv::imencode(color_ext, color, color_bmp_buf);
    cv::imencode(depth_ext, depth, depth_enc_buf);

    // Compress color file to memory buffer
    long comp_color_size = LZ4_compressBound(color_bmp_buf.size() + 8 * 1024);
    std::shared_ptr<char[]> comp_color_buf(new char[comp_color_size]);
    long comp_size = LZ4_compress_fast(
            (const char *)(color_bmp_buf.data()), comp_color_buf.get(),
            color_bmp_buf.size(), comp_color_size, 2);

    std::shared_ptr<char[]> comp_depth_buf(new char[depth_enc_buf.size()]);
    std::memcpy(comp_depth_buf.get(), depth_enc_buf.data(), depth_enc_buf.size());

    FileEntry color_file{std::move(rgb_filename), std::move(comp_color_buf), comp_size};
    FileEntry depth_file{std::move(dep_filename), std::move(comp_depth_buf), long(depth_enc_buf.size())};
    write_buf.emplace(std::move(color_file));
    write_buf.emplace(std::move(depth_file));

    //Release memory in reverse order
    color.release();
    depth.release();

    return 0;
}

void ImgWriter::startWriterThreads(int num) {
    auto body = []() {
        for (; Device::should_run || !write_buf.empty(); )
        {
            FileEntry des;
            for (; !write_buf.try_pop(des); )
            { std::this_thread::sleep_for(std::chrono::milliseconds(12));}
            int fd = open(des.filename.c_str(), O_WRONLY | O_CREAT, 0666);
            int rtn = write(fd, des.content_buf.get(), des.bufsize);
            if (rtn == -1) {
                std::cout << "Unable to write file " << des.filename << std::endl;
                std::cout << strerror(errno) << std::endl;
                throw "Unable to write file";
            }
            close(fd);
        }
    };

    auto fwrite_body = []() {
        FILE * handle = fopen("/home/liyanc/Desktop/ssd/bigfile.bin", "w+");
        for (; Device::should_run || !write_buf.empty(); )
        {
            FileEntry des;
            for (; !write_buf.try_pop(des); ) { }
            fwrite(des.content_buf.get(), 1, des.bufsize, handle);
        }
        fclose(handle);
    };

    for (int i = 0; i < num; ++i)
        writer_pool.emplace_back(body);
}

void ImgWriter::stopWriterThreads() {
    for (auto & t : writer_pool)
    {
        auto tid = t.get_id();
        t.join();
        std::cout << "Terminated writer thread " << tid << std::endl;
    }
}

int ImgWriterDumpJpeg::operator()(const TimedFrame &in_frame)
{
    char fname_color[32], fname_depth[32];
    std::string color_ext(".jpg");
    std::string depth_ext(".tiff");
    std::string compr_ext(".lz4");

    long current_cnt = ++counter;
    long time = boost::chrono::duration_cast<boost::chrono::milliseconds>(in_frame.timestamp).count();

    sprintf(fname_color, "%02ld-%09ld-%09ld", in_frame.kinect_ind, time, current_cnt);
    sprintf(fname_depth, "%02ld-%09ld-%09ld", in_frame.kinect_ind, time, current_cnt);

    std::string rgb_filename = work_dir + "/" + fname_color + color_ext;
    std::string dep_filename = work_dir + "/" + fname_depth + "_dep" + depth_ext;

    //FileEntry color_file{rgb_filename, {}};
    //FileEntry depth_file{dep_filename, {}};

    cv::Mat depth_ref(in_frame.depth_h, in_frame.depth_w, CV_32FC1, in_frame.depth_buf.get());

    //Convert frames to writing format
    cv::Mat depth;
    depth_ref.convertTo(depth, CV_16U);

    // Directly write to file
    //cv::imwrite(rgb_filename, color, {CV_IMWRITE_PNG_COMPRESSION, 1});
    //cv::imwrite(dep_filename, depth);

    // Write encoded bytestreams to queue
    // Magic number for 2^18 * 9.3125 and 2^14 * 4.25

    //depth_file.content.reserve(69632);
    std::vector<uchar> depth_enc_buf;
    depth_enc_buf.reserve(69632);

    cv::imencode(depth_ext, depth, depth_enc_buf);

    // Compress color file to memory buffer

    std::shared_ptr<char[]> comp_depth_buf(new char[depth_enc_buf.size()]);
    std::shared_ptr<char[]> raw_color_buf = std::reinterpret_pointer_cast<char[]>(in_frame.color_buf);

    std::memcpy(comp_depth_buf.get(), depth_enc_buf.data(), depth_enc_buf.size());

    FileEntry color_file{std::move(rgb_filename), std::move(raw_color_buf), in_frame.jpeg_buf_len};
    FileEntry depth_file{std::move(dep_filename), std::move(comp_depth_buf), long(depth_enc_buf.size())};
    write_buf.emplace(std::move(color_file));
    write_buf.emplace(std::move(depth_file));

    //Release memory in reverse order
    depth.release();

    return 0;
}

ImgWriterDumpJpeg::ImgWriterDumpJpeg(const std::string &parentDir, const std::string &sess)
    : ImgWriter(parentDir, sess) {}
