//
// Created by liyanc on 8/15/18.
//

#ifndef KINECT_CAPTURE_TIMESTAMPWRITER_H
#define KINECT_CAPTURE_TIMESTAMPWRITER_H

#include <cstdio>
#include <tbb/flow_graph.h>

#include "Device.h"

class TimestampWriter {
private:
    FILE * file;

public:
    TimestampWriter(const std::string &filename);

    int operator() (const TimedFrame & in_frame);

    void closeFile();

    //virtual ~TimestampWriter();
};


#endif //KINECT_CAPTURE_TIMESTAMPWRITER_H
